from email.policy import default
import json
from datetime import datetime
from bs4 import BeautifulSoup


def get_buy_sell(prices):
    return prices[0]["data-currencies-rate-buy"], prices[1]["data-currencies-rate-sell"]


with open("index.html", "r") as page, open("prices.json", "r") as prices_file:
    bs = BeautifulSoup(page, "html.parser")
    prices = json.load(prices_file)

    run_time = datetime.now().replace(microsecond=0)

    data_row_items = bs.find_all("div", attrs={"data-row-item": True})
    for exchange in data_row_items:
        name = " ".join(exchange.findChildren("a")[0].text.split())
        address = " ".join(
            exchange.findChildren("div", class_="font-size-small")[0].text.split()
        )
        usd_prices = exchange.findChildren("div", attrs={"data-currencies-code": "USD"})
        eur_prices = exchange.findChildren("div", attrs={"data-currencies-code": "EUR"})
        usd_buy, usd_sell = get_buy_sell(usd_prices)
        eur_buy, eur_sell = get_buy_sell(eur_prices)

        prices_in_exchange = prices.setdefault(name, {}).setdefault(
            address,
            [
                {
                    "datetime": datetime.isoformat(run_time),
                    "usd_buy": usd_buy,
                    "usd_sell": usd_sell,
                    "eur_buy": eur_buy,
                    "eur_sell": eur_sell,
                }
            ],
        )

        prices_in_exchange.sort(
            reverse=True, key=lambda x: datetime.fromisoformat(x["datetime"])
        )
        last_prices = prices_in_exchange[0]
        if datetime.fromisoformat(last_prices["datetime"]) != run_time:
            usd_buy_change = float(usd_buy) - float(last_prices["usd_buy"])
            if usd_buy_change > 1e-6:
                print(
                    f'USD buy price in {name} on {address} changed from { last_prices["usd_buy"] }, to {usd_buy}, change {usd_buy_change}'
                )
            usd_sell_change = float(usd_sell) - float(last_prices["usd_sell"])
            if usd_sell_change > 1e-6:
                print(
                    f'USD buy price in {name} on {address} changed from { last_prices["usd_sell"] }, to {usd_sell}, change {usd_sell_change}'
                )
            eur_buy_change = float(eur_buy) - float(last_prices["eur_buy"])
            if eur_buy_change > 1e-6:
                print(
                    f'EUR buy price in {name} on {address} changed from { last_prices["eur_buy"] }, to {eur_buy}, change {eur_buy_change}'
                )
            eur_sell_change = float(eur_sell) - float(last_prices["eur_sell"])
            if eur_sell_change > 1e-6:
                print(
                    f'USD buy price in {name} on {address} changed from { last_prices["eur_sell"] }, to {eur_sell}, change {eur_sell_change}'
                )

            prices[name][address].append(
                {
                    "datetime": datetime.isoformat(run_time),
                    "usd_buy": usd_buy,
                    "usd_sell": usd_sell,
                    "eur_buy": eur_buy,
                    "eur_sell": eur_sell,
                }
            )
    with open("prices.json", "w") as prices_file:
        json.dump(prices, prices_file, ensure_ascii=False, default=str)
