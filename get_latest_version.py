import sys
import requests

api_address = sys.argv[1]
project = sys.argv[2]
package = sys.argv[3]
token = sys.argv[4]

packages = requests.get(
    f"{api_address}/projects/{project}/packages?package_name={package}&order_by=version&sort=desc",
    headers={"JOB-TOKEN": token},
).json()
if packages:
    exit(0)
else:
    SystemError()
